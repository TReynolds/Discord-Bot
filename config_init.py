import json

def getList(listName):
    with open('config/'+listName+'.json') as filein:
        data = json.load(filein)
    return data[listName]

def getPrefix():
    return configData['prefix']

def setPrefix(new_prefix):
    configData['prefix'] = new_prefix
    writeToIni()

def getClientID():
    return configData['clientID']

def getOwnerID():
    return configData['ownerID']

def setOwnerID(new_id):
    configData['ownerID'] = new_id

def writeToIni():
    f = open('config/config.ini', 'w')
    for keys in configData:
        f.write('{}={}\n'.format(keys, configData[keys]))
    f.close()

def splitValues(file_path):
    f = open(file_path,'r')
    fData = f.read()
    fData = fData.split()
    configDictionary = {}
    for info in fData:
        info = info.split('=')
        setting = info[0]
        val = info[1]
        configDictionary[setting] = val
    f.close()
    return configDictionary


configData = splitValues('config/config.ini')
