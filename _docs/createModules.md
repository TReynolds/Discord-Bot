# Creating Modules / Cogs
## Quick Information
 - The words 'module' and 'cogs' are the same thing in this document and will be switched out frequently with each other.  
 - Cog is the correct term and is what is needed in each file to run.  
 - All modules are stored in:
    ``` Discord-Bot/modules/ ```  
 - Each module is a simple python file set up with the commands and actions. A restart of the bot is required to load the new module / cog.

## File Setup
Each new module needs to follow this format.  

#### Cog with a command
```py

from discord.ext import commands

class cogName():
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command()
    async def cogCommand(self):
        """
            Cog stuff here
        """
        await self.bot.say('new command added')

def setup(bot):
    bot.add_cog(cogName(bot))

```

#### Cog that reacts to any message
```py

from discord.ext import commands

class cogName():
    def __init__(self, bot):
        self.bot = bot
    
    async def on_message(self, message):
        """
            Cog stuff here
        """
        msg = "Whatever the bot says..."
        await self.bot.send_message(message.channel, msg)

def setup(bot):
    bot.add_cog(cogName(bot))

```