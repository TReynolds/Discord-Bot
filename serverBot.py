print('Turning on...\nPlease Standbye\n')

import discord
import random

from discord.ext import commands
from discord.ext.commands import Bot
from discord.voice_client import VoiceClient

from config_init import *
from modules import *
from os import listdir
from os.path import isfile, join

import asyncio

client_id = getClientID()

tokenFile = open("config/secret.config")
TOKEN = tokenFile.read()
tokenFile.close()

module_dir = "modules"

bot = commands.Bot(command_prefix=getPrefix())

@bot.event
async def on_ready():
    game = discord.Game(name="with my friends :D", type=0)
    await bot.change_presence(game=game)
    print("\n\nI am online and ready...")
    print("I am running as " + bot.user.name)
    print("With ID: " + bot.user.id)
    print('-'*50)
    print('Invite me into your server: https://discordapp.com/oauth2/authorize?client_id={}&scope=bot'.format(client_id))


@bot.command(pass_context=True)
async def hello(ctx):
    greets = getList("greetings")
    await bot.say(greets[random.randint(0, len(greets)-1)] + ctx.message.author.mention)

@bot.command(pass_context=True)
async def goodbye(ctx):
    farewells = getList("farewells")
    await bot.say(farewells[random.randint(0, len(farewells)-1)] + ctx.message.author.mention)


if __name__ == "__main__":
    for extension in [f.replace('.py', '') for f in listdir(module_dir) if isfile(join(module_dir, f))]:
        if extension not in ["__init__", "__pycache__"]:
            print("Loading extension {}".format(extension))
            try:
                bot.load_extension(module_dir + "." + extension)
                print('Loaded {}\n'.format(extension))
            except Exception as e:
                print('Failed to load extension {}.'.format(extension))
                print(e)

    bot.run(TOKEN)
