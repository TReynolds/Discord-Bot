Discord Bot
===========

## How To Run This Bot

### Install & Run Off Local Machine

#### Python
You will need python 3.6 or higher. You can download python from here...  
https://www.python.org/downloads/  



#### Pip
Once python is installed, you will need to install pip. You can do this via
the command line after python has been installed.  

    python get-pip.py


#### Discord.py
You can follow the linked guide on how to install Discord.py:  
https://github.com/Rapptz/discord.py/blob/async/README.md


#### Creating A Bot
Follow this link to create your own bot account for this to run from.  
https://discordapp.com/developers/applications/  
Once the account is created, record your clientID and your secret token.  
These will be used for running the bot.

#### Install and Run
Download this Repo onto your machine and navigate to the config folder.  
``` C:../../../Discord-Bot/config ```

Inside the config.ini file, place your replace the clientID with the one
you generated.

In the same folder, create a file called 'secret.config' and paste your  
secret token inside this file.

Navigate to:  
``` ../Discord-Bot ```

and run bot-main.py. Copy and paste the link from the console into a browser to invite the bot to a server.

#### Create Cogs
You can either look at the pre-existing mods and follow along or read the guide [here...](/_docs/createModules.md)