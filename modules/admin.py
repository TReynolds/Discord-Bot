from discord.ext import commands
from config_init import getClientID, setPrefix, getOwnerID

class Admin():

    def __init__(self, bot):
        self.bot = bot

    @commands.command(brief="Output invite link for the bot")
    async def botLink(self):
        await self.bot.say('Use This Link To Bring Me Into Another Server :D\n\nhttps://discordapp.com/oauth2/authorize?client_id={}&scope=bot'.format(getClientID()))

    @commands.command(brief="Change the bots prefix", pass_context=True)
    async def modPrefix(self, ctx, arg):
        user = ctx.message.author.id
        if user == getOwnerID():
            setPrefix(arg)
            await self.bot.say('Prefix Has Been Changed To {}'.format(arg))
            await self.bot.say('Please Reboot The Bot')
        else:
            await self.bot.say('You Are Not Authorized To Do This.\nPlease Contact The Bot Owner')



def setup(bot):
    bot.add_cog(Admin(bot))
