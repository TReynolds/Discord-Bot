import urllib.request
from discord.ext import commands

class ExtIP():
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def getIP(self):
        msg = await self.bot.say('This May Take A Couple Of Seconds...')
        external_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')
        await self.bot.edit_message(msg, 'My Current IP Address Is: {}'.format(external_ip))

def setup(bot):
    bot.add_cog(ExtIP(bot))
