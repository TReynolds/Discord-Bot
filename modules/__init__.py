import os

dir = os.listdir('modules/')

__all__ = [x.split('.')[0] for x in dir]


print('Found Modules: ')
for module in __all__:
    if module not in ["__init__", "__pycache__"]:
        print('  --{}'.format(module))

print('\n')
