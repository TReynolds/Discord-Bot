from discord.ext import commands

class Unflip():
    def __init__(self, bot):
        self.bot = bot


    async def on_message(self, message):
        flippedTable = "(╯°□°）╯︵ ┻━┻"
        if flippedTable in message.content:
            messageTemp = message.content
            tableCount = messageTemp.count(flippedTable)
            for x in range(tableCount):
                await self.bot.send_message(message.channel, "┬─┬ノ( º _ ºノ)")

def setup(bot):
    bot.add_cog(Unflip(bot))
